FROM node:15

WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install dotenv express cpu-benchmark cors
COPY . .
EXPOSE 3000

CMD [ "node", "app.js" ]